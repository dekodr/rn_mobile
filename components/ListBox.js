import React from 'react';
import PropTypes from 'prop-types';
import { 
  View
  , Text
  , Image
  , StyleSheet
  , Platform } from 'react-native';

class ListBox extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    location: PropTypes.string.isRequired,
    time: PropTypes.string.isRequired,
    distance: PropTypes.string.isRequired,
  };

	render() {
    const { name, location, time, distance } = this.props;
		return (
			<View style={styles.listBox}>
        <View style={styles.spanDistance}>
          <Text style={styles.spanText}>{distance}</Text>
        </View>
        <View style={styles.listImageWp}>
          <Image
            style={styles.listImage}
            source={require('../assets/store-1.png')}
          />
        </View>
        <View style={styles.boxContent}>
          <Text style={styles.name}>{name}</Text>
          <Text style={styles.location}>{location}</Text>
          <Text style={styles.time}>{time}</Text>
        </View>
      </View>
		)
	}
}

export default ListBox;

const styles = StyleSheet.create({
  listBox: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginVertical: 8,
  },
  listImageWp: {
    width: 72,
    height: 72,
    borderRadius: 6,
    overflow: 'hidden',
    position: 'absolute',
    left: 0,
    zIndex: 9,
  },
  listImage: {
    width: '100%',
    height: '100%',
  },
  boxContent: {
    position: 'relative',
    paddingVertical: 15,
    paddingHorizontal: 35,
    backgroundColor: '#F6F6F6',
    borderRadius: 6,
    zIndex: 0,
    flex: 1,
    marginLeft: 55,
  },
  name: {
    fontSize: 17,
    fontFamily: 'CeraPro-Bold',
    color: '#2A3348',
  },
  location: {
    fontSize: 13,
    fontFamily: 'CeraPro-Regular',
    color: '#2A3348',
  },
  time: {
    fontSize: 13,
    fontFamily: 'CeraPro-Regular',
    color: '#798191',
  },
  spanDistance: {
    position: 'absolute',
    right: 0,
    top: -8,
    paddingVertical: 3,
    paddingHorizontal: 8,
    backgroundColor: '#50D6B6',
    borderRadius: 25,
    borderTopRightRadius: Platform.OS === 'ios' ? 10 : 25,
    borderBottomRightRadius: 4,
    zIndex: 9,
  },
  spanText: {
    fontSize: 13,
    color: '#ffffff',
    fontFamily: 'CeraPro-Bold',
  },
});