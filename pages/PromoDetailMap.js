import React from 'react';
import PropTypes from 'prop-types';
import { 
  View
  , Text
  , Button
  , StyleSheet
  , Dimensions 
  , TouchableOpacity
  , Image
  , ScrollView
  , SafeAreaView
  , Fragment } from 'react-native';

import ListBox from '../components/ListBox';

let ScreenHeight = Dimensions.get("window").height;

class PromoDetailMap extends React.Component {
  static navigationOptions = {
    header: null
  }

  render() {
    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };
    return (
      <View>
        <View style={styles.mapWrapper}>
          <Image
            style={{width: '100%', height: '100%'}}
            source={require('../assets/map-background.png')}
          />
          <Image
            style={Object.assign({}, mapMarker, {top: 155, left: 55}) }
            source={require('../assets/map-marker.png')}
          />
          <Image
            style={Object.assign({}, mapMarker, {top: 95, left: 175}) }
            source={require('../assets/map-marker.png')}
          />
        </View>
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          <View style={styles.mcGrid}>
            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-bookmark.png')}
                />
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-share.png')}
                />
              </View>
            </View>
            <ScrollView style={[styles.mcGrid, styles.pageWrapper]}>
              <View style={{width: '100%', height: 350, backgroundColor: 'rgba(0,0,0,0)'}}></View>
              <View style={styles.pageHeader}>
                <View style={styles.headerImage}>
                  <Image
                    style={{width: 60, height: 60}}
                    source={require('../assets/brand-lays.png')}
                  />
                </View>
                <Text style={styles.headerTitle}>Lay's Store</Text>
                <Text style={styles.headerSubtitle}>6 stores near you</Text>
              </View>
              <View style={styles.pageContent}>
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
      </View>
    );
  }
}

export default PromoDetailMap;

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 95,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 5,
    fontSize: 15,
    color: '#2A3348',
  },

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
  },

  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 150,
    paddingHorizontal: 15,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    marginBottom: 300,
  },

});