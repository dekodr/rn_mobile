import React from 'react';
import PropTypes from 'prop-types';
import { 
  View
  , Text
  , StyleSheet
  , Dimensions 
  , TouchableHighlight
  , TouchableOpacity
  , Image
  , ScrollView
  , TextInput
  , SafeAreaView 
  , Linking} from 'react-native';

  import Modal from "react-native-modal";

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import NumberFormat from 'react-number-format';

import ListBox from '../components/ListBox';
import Button from '../components/Button';

import {
     black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

let ScreenHeight = Dimensions.get("window").height;

class PayReceipt2 extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    search     : '',
    receipt_id : '',
    loading    : true,
    orderList  : [],
    swipeablePanelActive : false,
    modalVisible : false,
    selectedProduct: '',
    selectedURL: '',
    selectedImage: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  openPanel(product, link, image){
    if(link == '' || link == 'http://'){
      return false;
    }else{
      this.setState({ isModalVisible: !this.state.isModalVisible, selectedProduct: product, selectedURL: link, selectedImage: image });
    }
    
  }

  closePanel =() =>{
    this.setState({ swipeablePanelActive: false });
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  };

  openURL = () => {
    
    Linking.canOpenURL(this.state.selectedURL).then(supported => {
      if (supported) {
        Linking.openURL(this.state.selectedURL);
      } else {
        console.log("Don't know how to open URI: " + this.state.selectedURL);
      }
    });
  };


  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  // Load Data to retrieve in page
  async componentDidMount(){
    try {
      let response = await fetch(
        'http://103.3.62.188/osmosis/getData_/'+this.props.navigation.state.params.receipt_id,
      );
      // +this.props.navigation.state.params.receipt_id
      let responseJson = await response.json();

      this.setState({orderList: responseJson, loading: false});
      console.log("orderlist:")
      console.log(this.state.orderList);

    }catch (error) {
      console.error(error);
    }
    console.log("component mount");
  }

  
  renderOrderList() {
    const productImg = {
      25272: require('../assets/product/25272.jpg'),
      25273: require('../assets/product/25273.jpg'),
      25274: require('../assets/product/25274.jpg'),
      25275: require('../assets/product/25275.jpg'),
      25276: require('../assets/product/25276.jpg'),
      25277: require('../assets/product/25277.jpg'),
      25278: require('../assets/product/25278.jpg'),
      25279: require('../assets/product/25279.jpg'),
      25280: require('../assets/product/25280.jpg'),
      25281: require('../assets/product/25281.jpg'),
      25282: require('../assets/product/25282.jpg'),
      25283: require('../assets/product/25283.jpg'),
      25284: require('../assets/product/25284.jpg'),
      25285: require('../assets/product/25285.jpg'),
      25286: require('../assets/product/25286.jpg'),
      25287: require('../assets/product/25287.jpg'),
      // 25288: require('../assets/product/25288.jpg'),
      25271: require('../assets/product/25271.jpg'),
      25270: require('../assets/product/25270.jpg'),
      25269: require('../assets/product/25269.jpg'),
      25268: require('../assets/product/25268.jpg'),
      25267: require('../assets/product/25267.jpg'),
      25266: require('../assets/product/25266.jpg'),
      25262: require('../assets/product/25262.jpg'),
      25261: require('../assets/product/25261.jpg'),
      25260: require('../assets/product/25260.jpg'),
      25259: require('../assets/product/25259.jpg'),
      25258: require('../assets/product/25258.jpg'),
      25265: require('../assets/product/25265.jpg'),
      25264: require('../assets/product/25264.jpg'),
      25263: require('../assets/product/25263.jpg'),
    }
    
    let data = this.state.orderList.detail;
    console.log(data)
    return (<View>
                { data.map((item, key)=>(
                  <TouchableOpacity 
                  style={styles.ReceiptItem}>
                    <View style={styles.ReceiptIcon}>
                      <Image
                        style={{width: 50, height: 50}}
                        source={productImg[item.product_id]}/>
                    </View>
                    
                    <View style={styles.historyName}>
                      {/* BUTTON TOUCHABLE TO OPEN WEBSITE IF ANY */}
                        {item.website == null ?  <Text style={styles.ReceiptSubtitle}>{item.product_name}</Text> : <TouchableOpacity onPress={() => { this.openPanel(item.product_name, item.website, item.product_id); }}><Text style={styles.ReceiptSubtitleBold}>{item.product_name}</Text></TouchableOpacity>}
                    </View>
                    <View style={styles.amountWp}>
                      <View style={styles.ReceiptAmount}>
                        <Text style={styles.ReceiptAmountText}>x{item.total_item}</Text>
                      </View>
                      <View style={styles.ReceiptPrice}>
                        <Text style={styles.ReceiptPriceText}>
                          <NumberFormat value={item.total_price} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} renderText={value => <Text>{value}</Text>}/>
                        </Text>
                      </View>
                    </View>
                  </TouchableOpacity>
                ))}

          </View>
          );
  }
  
  redirectToPayment(){
    console.log(this.state.orderList.transaction_total)

    this.props.navigation.navigate('PayEnterPin', { transaction_total:this.state.orderList.transaction_total })
  }


  render() {
    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    const { search }      = this.state;
    const { receipt_id }  = this.props.navigation.state.params.receipt_id;
    let id_ = this.state.selectedImage.toString();

   
    
    if(this.state.loading == true){
      return (
        <View></View>
      );
    }else{
      return (
        <SafeAreaView style={{ paddingBottom: 125 }}>
          <View style={styles.mcGrid}>
            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableHighlight 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.navigate('History')}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableHighlight>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>
            <ScrollView style={[styles.mcGrid, styles.pageWrapper]}>
              <View style={{width: '100%', height: 10, backgroundColor: 'rgba(0,0,0,0)'}}></View>

              <View style={[styles.mcRow, styles.firstRow, styles.header, styles.center]}>
                <Text style={styles.title}>Order #{this.state.orderList.id}</Text>
                <Text style={styles.subtitle}>Just make sure everything is right!</Text>
              </View>

              {/* LIST ITEM */}
              <View style={[styles.mcRow, styles.center, styles.historyWrapper]}>
                {/* LOAD FROM renderOrderList */}
                {this.renderOrderList()}
                
                <View style={[styles.mcRow, styles.listTax]}>
                  <View style={[styles.mcCol6]}>
                    <Text style={[styles.alignLeft, styles.total]}>Total</Text>
                  </View>
                  <View style={[styles.mcCol6]}>
                    <Text style={[styles.alignRight, styles.total]}>
                      
                      <NumberFormat value={this.state.orderList.transaction_total} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} renderText={value => <Text>{value}</Text>}/>
                    </Text>
                  </View>
                </View>
                <View style={[styles.mcRow, styles.listTax]}>
                  <View style={[styles.mcCol6]}>
                    <Text style={[styles.alignLeft, styles.total]}>Point</Text>
                  </View>
                  <View style={[styles.mcCol6]}>
                    <Text style={[styles.alignRight, styles.total]}>{Math.round(this.state.orderList.transaction_total * 0.003)}</Text>
                  </View>
                </View>
              </View>

              <View style={[styles.mcRow, styles.bottomButton]}>
                <View style={styles.mcCol1}>
                  <Button 
                    text="Proceed to Pay"
                    size="large"
                    theme="primary"
                    onPress={() => this.redirectToPayment()}
                  />
                </View>
              </View>

            </ScrollView>
          </View>


          {/* Modal Area */}  
          <Modal isVisible={this.state.isModalVisible}>
            <View style={{ backgroundColor: '#fff', height: 125, width: '100%', borderRadius: 16, paddingTop: 45, paddingHorizontal: 25, paddingBottom: 25}}>
              <TouchableOpacity 
                style={page.closeButton}
                onPress={this.toggleModal}>
                  <Image
                    style={{width: 10, height: 10}}
                    source={require('../assets/icon-times-white.png')}
                  />
              </TouchableOpacity>
              <View style={page.rowButton}>
                <TouchableOpacity style={page.ReceiptButton}>
                  <Text style={page.ReceiptButtonText}>Find Info</Text>
                </TouchableOpacity>
                <TouchableOpacity 
                style={[page.ReceiptButton, {backgroundColor: '#57789F'}]} 
                onPress={() => this.openURL(this.state.selectedURL)}>
                  <Text style={page.ReceiptButtonText}>Find Recipes</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          {/* Modal Area - - - END */}
        </SafeAreaView>
      );
    }
  }
}

export default PayReceipt2;

const styles = StyleSheet.create({

  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: '#36A49F',
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  ReceiptIcon: {
    height: 50,
    width: 50,
    borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
    backgroundColor: '#ffffff',
  },
  ReceiptItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: '#f6f6f6',
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  ReceiptTitle: {
    fontSize: 16,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
    width: wp('30%'),
  },
  ReceiptSubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: '#2A3348',
    width: wp('30%'),
  },
  ReceiptSubtitleBold: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: '#2A3348',
    width: wp('30%'),
    fontWeight: 'bold'
  },
  ReceiptPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: '#36A49F',
    fontFamily: 'CeraPro-Bold',
  },
  amountWp: {
    flexDirection: 'column',
    alignSelf: 'flex-end',
    justifyContent: 'flex-end',
    marginLeft: 'auto',
  },
  ReceiptAmount: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptAmountText: {
    textAlign: 'right',
    fontSize: 13,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },

  contentList: {
    marginVertical: 35,
    flexDirection: 'column',
  },
  listDefault: {
    width: '100%',
    flexDirection: 'row',
    height: 55,
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderColor: '#dddddd',
    marginTop: 10,
  },
  listAmount: {
    fontSize: 19,
    fontWeight: '700',
    letterSpacing: 2,
    color: '#2A3348',
  },
  alignLeft: {
    textAlign: 'left',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  alignRight: {
    textAlign: 'right',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  total: {
    fontSize: 20,
    color: '#36A49F',
  },
  listName: {
    fontSize: 14,
    color: '#2A3348',
    textAlign: 'left',
  },
  listPrice: {
    fontSize: 17,
    fontWeight: '700',
    color: '#36A49F',
  },
  listTax: {
    marginTop: 5,
  },
  bottomButton: {
    marginHorizontal: 5,
    paddingTop: 10,
    paddingBottom: 150,
  },

  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
    backgroundColor: '#ffffff',
  },
  mcRow: {
    marginVertical: 0,
    // marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 45,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 5,
    fontSize: 15,
    color: '#2A3348',
  },

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  center: {
    paddingHorizontal: 25,
  },
  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 225,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontWeight: '700',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    marginBottom: 100,
  },

  // - - - -
  header: {
    flexDirection: 'column',
  },
  title: {
    fontSize: 29,
    fontWeight: '700',
    color: '#2A3348',
  },
  subtitle: {
    fontSize: 16,
    color: '#798191',
  },

  search: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
    borderRadius: 8,
    height: 45,
    borderRadius: 8,
    marginVertical: 24,
    marginHorizontal: 35, 
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: '#ffffff',
    paddingHorizontal: 15,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 4 },
    // shadowOpacity: 0.3,
    // shadowRadius: 4,
    // elevation: 10,
    // borderRadius: 8,
  },
  searchInput: {
    flex: 1,
    height: 45,
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f6f6f6',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    height: 45,
  },

});

const page = StyleSheet.create({

  rowButton: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  ReceiptButton: {
    width: '47%',
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#77599A',
    borderRadius: 8,
  },
  ReceiptButtonText: {
    fontSize: 17,
    fontFamily: 'CeraPro-Bold',
    color: '#ffffff',
  },

  top: {
    zIndex: 3,
  },
  closeButton: {
    height: 25,
    width: 25,
    backgroundColor: 'rgba(211,62,62,1)',
    borderRadius: 25,
    position: 'absolute',
    right: 7,
    top: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },

  listTax: {
    marginTop: 5,
  },
  alignLeft: {
    textAlign: 'left',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  alignRight: {
    textAlign: 'right',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  total: {
    fontSize: 20,
    color: '#36A49F',
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerDate: {
    fontSize: 15,
    fontFamily: 'CeraPro-Regular',
    color: grey,
    letterSpacing: 1.2,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  ReceiptIcon: {
    height: 50,
    width: 50,
    borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  ReceiptItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  ReceiptTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
    width: wp('30%'),
  },
  ReceiptSubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: black,
    width: wp('30%'),
  },
  ReceiptPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});