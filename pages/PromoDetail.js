import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity
  , SafeAreaView } from 'react-native';

import Button from '../components/Button';
import PromoBox1 from '../components/PromoBox1';

class PromoDetail extends React.Component {
  static navigationOptions = {
    header: null
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={styles.mcGrid}>
          <View style={[styles.mcRow, styles.topNavigation]}>
            <TouchableOpacity 
              style={[styles.mcCol1, styles.backButton]}
              onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{width: 35, height: 35}}
                source={require('../assets/icon-chevron-left.png')}
              />
            </TouchableOpacity>
            <View style={[styles.mcCol4, styles.topNavTitle]}>
              <Text style={styles.pageTitle}></Text>
            </View>
            <View style={[styles.mcCol3, styles.leftIcons]}>
              <Image
                style={{width: 35, height: 35}}
                source={require('../assets/icon-bookmark.png')}
              />
              <Image
                style={{width: 35, height: 35}}
                source={require('../assets/icon-share.png')}
              />
            </View>
          </View>
          <ScrollView style={[styles.mcGrid, styles.modalWrapper]}>
            <View style={styles.modalImage}>
              <Image
                style={{width: '100%', height: 250}}
                source={require('../assets/promo-2-1.png')}
              />
            </View>
            <View style={styles.modalContent}>
              <View style={styles.modalContentLogo}>
                <Image
                  style={{width: 75, height: 75}}
                  source={require('../assets/brand-lays.png')}
                />
              </View>
              <View style={styles.modalContentMain}>
                <Text style={styles.modalTitle}>Buy 1 get 1 FREE !</Text>
                <Text style={styles.modalParagraph}>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.  Lorem ipsum dolor sit amet.  </Text>

                <View style={styles.timeSection}>
                  <Image
                    style={{width: 32, height: 32}}
                    source={require('../assets/icon-calendar.png')}
                  />
                  <Text style={styles.timeSectionText}>24 Mei 2019 - 30 Juni 2019</Text>
                </View>

                <Text style={styles.modalTextDivider}>Avilable in</Text>

                <View style={[styles.sliderBox]}>
                  <ScrollView 
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                  >
                    <PromoBox1 
                      text="Store title #1"
                    />
                    <PromoBox1 
                      text="Store title #2"
                    />
                    <PromoBox1 
                      text="Store title #3"
                    />
                    <PromoBox1 
                      text="Store title #4"
                    />
                    <PromoBox1 
                      text="Store title #5"
                    />
                    <PromoBox1 
                      text="Store title #6"
                      lastSlide
                    />
                  </ScrollView>
                </View>

                <Text style={styles.modalTextDivider}>Terms & Conditions</Text>

                <View style={styles.list}>
                  <View style={styles.listItem}>
                    <View style={styles.listStyle}></View>
                    <Text style={styles.listText}>Lorem ipsum dolor sit amet.</Text>
                  </View>
                  <View style={styles.listItem}>
                    <View style={styles.listStyle}></View>
                    <Text style={styles.listText}>Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet.</Text>
                  </View>
                  <View style={styles.listItem}>
                    <View style={styles.listStyle}></View>
                    <Text style={styles.listText}>Lorem ipsum dolor sit amet.</Text>
                  </View>
                </View>

                <View style={styles.modalButton}>
                  <Button 
                    text="View more promo from Lay's"
                    size="large"
                    theme="primary"
                    onPress={() => this.props.navigation.navigate('PromoDetailMap')} 
                  />
                </View>
              </View>
            </View>
          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default PromoDetail;

const styles = StyleSheet.create({
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 95,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  sliderDefault: {
    paddingHorizontal: 0,
    marginHorizontal: 0,
    height: 200,
  },
  sliderCard: {
    height: 75,
  },
  promoDefault: {
    height: 180, 
    width: 280, 
    marginLeft: 23,
    overflow: 'visible',
  },
  promoTitle: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 25,
    backgroundColor: 'rgba(255,255,255,0.8)',
    borderRadius: 16,
  },
  title: {
    fontSize: 17,
    color: '#2A3348',
    fontWeight: '700',
  },
  promoSpan: {
    position: 'absolute',
    right: 15,
    top: -8,
    fontSize: 10,
    paddingVertical: 3,
    paddingHorizontal: 9,
    backgroundColor: '#F4376A',
    borderRadius: 25,
    color: '#ffffff',
    textTransform: 'uppercase',
    letterSpacing: 3,
    fontWeight: '700',
  },
  lastSlide: {
    marginRight: 23,
  },

  // topNavigation
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  // modalWrapper
  modalWrapper: {
    marginTop: -65,
    position: 'relative',
    zIndex: 1,
    paddingBottom: 25,
  },
  modalImage: {
    position: 'relative',
    zIndex: 1,
  },
  modalContent: {
    flex: 1,
    marginTop: -45,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: '#ffffff',
    position: 'relative',
    zIndex: 9,
    paddingBottom: 75,
  },
  modalContentLogo: {
    height: 80,
    width: 80,
    borderRadius: 100,
    backgroundColor: 'rgba(255,255,255,.75)',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 35,
    top: -35,
    zIndex: 9,
  },
  modalContentMain: {
    marginTop: 75,
  },
  modalTitle: {
    fontSize: 28,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
    paddingHorizontal: 35,
  },
  modalParagraph: {
    fontSize: 14,
    color: '#798191',
    marginVertical: 15,
    lineHeight: 25,
    paddingHorizontal: 35,
  },
  timeSection: {
    alignItems: 'center',
    flexDirection: 'row',
    padding: 14,
    backgroundColor: '#F6F6F6',
    borderRadius: 12,
    marginVertical: 15,
    marginHorizontal: 35,
    position: 'relative',
    // aspectRatio: 1,
  },
  timeSectionText: {
    fontSize: 14,
    fontFamily: 'CeraPro-Bold',
    color: '#2A3348',
    marginLeft: 13,
    alignSelf: 'center',
    textAlign: 'center',
  },
  modalTextDivider: {
    fontSize: 22,
    fontFamily: 'CeraPro-Bold',
    color: '#2A3348',
    marginHorizontal: 35,
    marginTop: 15,
    marginBottom: 15,
  },
  list: {
    flex: 1,
    marginVertical: 15,
    marginHorizontal: 35,
  },
  listItem: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  listStyle: {
    width: 6,
    height: 6,
    borderRadius: 25,
    backgroundColor: '#798191',
    marginTop: 10,
  },
  listText: {
    fontSize: 14,
    color: '#798191',
    marginLeft: 8,
    lineHeight: 26,
  },
  modalButton: {
    marginHorizontal: 5,
    paddingTop: 75,
  },
});