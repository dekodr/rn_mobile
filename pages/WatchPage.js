import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , TouchableOpacity 
  , TextInput
  , Footer
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
import Modal from "react-native-modal";

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';

let ScreenHeight = Dimensions.get("window").height;

class WatchPage extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    return (
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          <View style={[styles.mcGrid, page.top]}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>
            <BoxAccent
              yAxis={-25}
              xAxis={325}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              yAxis={-200}
              xAxis={-100}
              width={300}
              height={300}
            ></BoxAccent>

            <View style={[styles.mcRow, page.center, page.header]}>
              <View style={styles.mcCol6}>
                <Text style={page.headerTitle}>Watch videos and get points!</Text>
              </View>
            </View>

            <View style={[styles.mcRow, page.center]}>
              <View style={page.bgImageWp}>
                <ImageBackground 
                  source={require('../assets/icon-watch.png')}
                  style={{height:280, width:'100%', borderRadius: 16, top: -30}}>
                </ImageBackground>
              </View>
            </View>

            <View style={[styles.row, page.center, {marginVertical: 15}]}>
              <Text style={page.caption}>
                lorem upsum dolor si ametlorem upsum dolor si ametlorem upsum dolor si amet
              </Text>
            </View>

            <View style={[styles.row, {marginTop: 75}]}>
              <Button 
                text="Get 30 Points"
                size="large"
                theme="primary2"
                onPress={this.toggleModal}
              />
            </View>

          </ScrollView>

          {/* Modal Area */}  

          {/* Modal Area - - - END */}
        </SafeAreaView>
    );
  }
};

export default WatchPage;

const page = StyleSheet.create({

  bgImageWp: {
    width: '100%',
    height: 200,
    borderRadius: 16,
    overflow: 'hidden',
    position: 'relative',
    marginVertical: 25,
  },
  caption: {
    fontSize: 16,
    fontWeight: '400',
    fontFamily: 'CeraPro-Regular',
    color: '#798191',
  },

  top: {
    zIndex: 3,
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  historyItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
  },
  historyTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
  },
  historySubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: darkGrey,
  },
  historyPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  historyPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});