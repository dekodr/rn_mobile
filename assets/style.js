import {
  StyleSheet,
  Dimensions
} from'react-native' 

import { normalize } from './Normalize';

let ScreenHeight = Dimensions.get("window").height;

const black = '#2A3348';
const primary = '#50D6B6';
const primary2 = '#36A49F';
const darkGrey = '#798191';
const grey = '#4B5760';
const lightGrey = '#f6f6f6';

const ceraReg = 'CeraPro-Regular';

const fonts = {
  CeraReg: 'ceraPro-Regular',
}

const styles = StyleSheet.create( {
  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },

  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 95,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 5,
    fontSize: 15,
    color: '#2A3348',
  },

  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  bold: {
    fontFamily: 'CeraPro-Bold',
  }
})

export { 
  styles
  , fonts
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey
}



