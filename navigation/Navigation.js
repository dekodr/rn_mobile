import React from 'react';
import { 
	StyleSheet
	, Text
	, View 
} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import HomeScreen from '../pages/Home';
import PromoDetailScreen from '../pages/PromoDetail';
import PromoDetailMapScreen from '../pages/PromoDetailMap';

const AppStackNavigator = createStackNavigator({
	Home: {
		screen: HomeScreen,
	},
	PromoDetail: {
		screen: PromoDetailScreen,
	},
	PromoDetailMap: {
		screen: PromoDetailMapScreen,
	},
}, 
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const App = createAppContainer(AppStackNavigator);

export default App;